/**
 * Created by joshua.fair on 6/6/2016.
 */
import {Component} from 'angular2/core';

@Component({
    templateUrl: 'app/products/product-detail.component.html'
})

export class ProductDetailComponent{
    pageTitle: string = 'Product Detail';
}